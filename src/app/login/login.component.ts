import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from '../services/http.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(private fb: FormBuilder,
             private cookieService: CookieService,
             private httpClient: HttpClient,
             private router: Router,
             private httpService: HttpService,
             private snackbar: MatSnackBar) { }



  submitLogin() {
    /*if form value is invalid */
    if (this.loginForm.valid) {

      const data = this.loginForm.value;
      const url = 'admin-login';

      // this.httpService.http({
      //   method: 'POST',
      //   url: 'admin-login',
      //   data: data
      // }).subscribe((response) => {
      //       console.log(response);
      //       console.log(response['body'])
      //       const res=response['body'];
      //       this.router.navigate(['dashboard']);
      //       this.cookieService.set('token', response['Token']);
      //   }, (error) => {
      //       console.log(error);
      //   })

      this.httpService.post(url, data).subscribe((response) => {
          console.log(response['result']);
          const permissions = JSON.stringify(response['result']);
          this.router.navigate(['dashboard']);
          this.cookieService.set('token', response.Token);
          this.cookieService.set('permissions', permissions);
          console.log(response);
      }, (error) => {
          console.log(error);

      });
    }
  }

  ngOnInit() {
      /*if token page redirect to dashboard page */
      if (this.cookieService.get('token')) {
        this.router.navigate(['dashboard']);
      }

    this.loginForm = this.fb.group({

      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

}
