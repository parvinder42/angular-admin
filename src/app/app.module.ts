import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent} from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CookieService } from 'ngx-cookie-service'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { DialogServiceComponent } from './services/dialog-service/dialog-service.component';
import { AuthInterceptor } from './config';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import { MatrialModule  } from './material.module';
import { PermissionService } from '../app/services/permission.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    FooterComponent,
    SidebarComponent,
    NavbarComponent,
    DialogServiceComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatrialModule
  ],
  entryComponents:[DialogServiceComponent],
  providers: [
    {provide:HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi:true },
    CookieService, PermissionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
