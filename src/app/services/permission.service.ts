import { Injectable, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Sidebar } from '../../app/components/sidebar/sidebar';
import { BehaviorSubject, Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PermissionService implements OnInit {

  permissionSidebar = [];
  displaySidebar = [];
  constructor(private cookieService: CookieService) { }

  routePermission() {
    console.log("check");
    this.permissionSidebar = JSON.parse(this.cookieService.get('permissions'));

    this.permissionSidebar.forEach((e1) => Sidebar.forEach((e2) => {

        if (e1 === e2.title) {
          this.displaySidebar.push(e2);
        }

        console.log(this.displaySidebar);
  
      }));
  }

    ngOnInit(){
      this.routePermission();

    }


}
