import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private apiCall = `${environment.apiUrl}`;

  constructor( private httpClient: HttpClient) { }

    http (request): Observable<any> {
      request.method = request.method ? request.method : 'GET';
      request.url = `${environment.apiUrl}${request.url}`;

      if (request.method === 'GET') {
        // const options = term ?
        // { params: new HttpParams().set('name', term) } : {};
       // request.data = request.data;
      }

      const req =  new HttpRequest(
        request.method,
        request.url,
        request.data
      );

      return this.httpClient.request(req);

    }
      post(url, data) {

        return this.httpClient.post<any>(this.apiCall + url, data);
      }

}
