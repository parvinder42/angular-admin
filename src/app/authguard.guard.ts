import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Sidebar } from '../app/components/sidebar/sidebar';
import { SidebarComponent } from '../app/components/sidebar/sidebar.component';
import { routes } from '../app/layout/layout.routing';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {

  permissionSidebar = [];
  displaySidebar = [];
  routeData = [];
  constructor(private cookieService: CookieService,
              private router: Router,
             // private sidebar: SidebarComponent,
              private route: ActivatedRoute) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      const token = this.cookieService.get('token');
      this.routeData = [];
      if (token && token !== 'undefined') {

      // this.sidebar.loadSidebar();

        // this.displaySidebar = [];
        // this.permissionSidebar = JSON.parse(this.cookieService.get('permissions'));
        // this.permissionSidebar.forEach((e1) => Sidebar.forEach((e2) => {

        //     if (e1 === e2.title) {
        //       this.displaySidebar.push(e2);
        //     }

        //   }));

        //   console.log(this.displaySidebar);

        //   this.displaySidebar.forEach((data) => {
        //     if (data.path) {
        //       console.log(data.path);
        //     } else if (data.submenu) {
        //       data.submenu.forEach((data2) => {
        //         console.log(data2.path.substr(1));

        //         console.log(routes);

        //         routes.forEach((routepath) => {
        //           console.log(routepath);
        //           if (data2.path.substr(1) === routepath.path)  {
        //             console.log(routepath);
        //             this.routeData.push(routepath);

        //           }

        //         });


        //       });
        //     }
        //     console.log(this.routeData);
        //   });

         return true;
      } else {
        this.router.navigate(['']);
        return false;
      }



  }
}
