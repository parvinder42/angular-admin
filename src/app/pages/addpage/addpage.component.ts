import { Component, OnInit , HostListener} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addpage',
  templateUrl: './addpage.component.html',
  styleUrls: ['./addpage.component.scss']
})
export class AddpageComponent implements OnInit {
  formAddContent: FormGroup;
  selectedFile;
 
  public options: Object = {

  
    charCounterCount: true,
    // Set the image upload parameter.
    imageUploadParam: 'image_param',
// 'https://s3.console.aws.amazon.com/s3/object/i-content/'
    // Set the image upload URL.
    imageUploadURL: 'http://localhost:3100/uploads/page',

    // Additional upload params.
    imageUploadParams: {id: 'my_editor'},

    // Set request type.
    imageUploadMethod: 'POST',

    // Set max image size to 5MB.
    imageMaxSize: 5 * 1024 * 1024,

    // Allow to upload PNG and JPG.
    imageAllowedTypes: ['jpeg', 'jpg', 'png'],

  };

  private pageAddWithImage = `${environment.apiUrl}/addPostWithImage`;
  private pageAdd = `${environment.apiUrl}/addPost`;
  constructor(private fb: FormBuilder, private httpClient: HttpClient, private router: Router) { }

    ele = document.querySelector('.file');

    onSelectedFile (file) {
      console.log(file.target.files[0]);
      if (file && file.target) {
         this.selectedFile = file.target.files[0];
      } else {
        this.selectedFile = null;
      }
    }


      addPage() {

        this.httpClient.post<any>(this.pageAdd, this.formAddContent.value).subscribe((response) => {
          console.log(response);
        }, (error) => {
          console.log(error);
        });

      }

      addPageWithFile() {

      // console.log(this.selectedFile);

      if (this.formAddContent.valid) {

        const formData = new FormData();

        formData.append('title', this.formAddContent.value.title);
        formData.append('type', this.formAddContent.value.type);
        formData.append('short_desc', this.formAddContent.value.short_desc);
        formData.append('meta_keyword', this.formAddContent.value.meta_keyword);
        formData.append('content', this.formAddContent.value.content);
        formData.append('meta_desc', this.formAddContent.value.meta_desc);
        formData.append('location', this.formAddContent.value.location);
        formData.append('order', this.formAddContent.value.order);
        formData.append('status', this.formAddContent.value.status);

        if ( this.selectedFile) {
          formData.append('file', this.selectedFile);

          this.httpClient.post<any>(this.pageAddWithImage, formData).subscribe((response) => {
            console.log(response);
            this.router.navigate(['pages']);
          }, (error) => {
            console.log(error);
          });
        } else {

          this.addPage();
        }
      }
    }


  ngOnInit() {
    this.formAddContent = this.fb.group({


      title: ['', [Validators.required]],
      short_desc: [''],
      content: [''],
      meta_keyword: [''],
      meta_desc: [''],
      location: [''],
      order: [''],
      status: ['', Validators.required],
      type: ['page']

    });
  }

}
