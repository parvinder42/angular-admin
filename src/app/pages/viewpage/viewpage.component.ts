import { Component, Inject, OnInit, ViewChild, OnChanges } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../../services/http.service';
import { Router } from '@angular/router';
import {MatPaginator} from '@angular/material';
import {PageEvent} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AddfaqComponent } from '../../faq/addfaq/addfaq.component';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogServiceComponent } from '../../services/dialog-service/dialog-service.component';


@Component({
  selector: 'app-viewpage',
  templateUrl: './viewpage.component.html',
  styleUrls: ['./viewpage.component.scss']
})
export class ViewpageComponent implements OnInit {


  displayedColumns: string[] = ['select', 'Title', 'Short_Desc', 'Meta_Keyword', 'Status', 'Action'];
  dataSource;
  totalLength;
  totalPage;
  CurrentPage;
  limit;
  selectedItem = [];
  pageEvent: PageEvent;


// page list api
private removeMultiPost = `${environment.apiUrl}removeMultiplePost`;
private removePost = `${environment.apiUrl}removeSinglePost`;

@ViewChild(MatPaginator) paginator: MatPaginator;
selection = new SelectionModel(true, []);


constructor(private httpClient: HttpClient,
  private dialog: MatDialog,
  private httpService: HttpService,
  private router: Router) { }

    // list of all pages
    pagelist() {

        const list = `${environment.apiUrl}postlist?type=page&page=${this.CurrentPage || 1}&limit=${this.limit || 5}`;

        this.httpClient.get<any>(list).subscribe((response) => {

          this.dataSource = response.data.data;
          this.dataSource.paginator = this.paginator;

          console.log(response.data.count.length);

          if (response.data.count.length > 0) {
            this.totalLength = response.data.count[0]['total'];
          } else {
            this.totalLength = 0;
          }
          console.log(response);
        }, (error) => {
          console.log(error);
        });

    }

    // remove the page
    removeSingleData(id) {

        const _id = {
            _id: id
          };

          const dialogRef = this.dialog.open(DialogServiceComponent, {
            width: '380px',
            height: '250px'
          });


          dialogRef.afterClosed().subscribe(result => {

            if (result === true) {

              this.httpClient.post<any>(this.removePost, _id).subscribe((response) => {

                    this.pagelist();

              }, (error) => {
                console.log(error);
              });
            }
          });
    }


    quickAction() {


      this.httpClient.post<any>(this.removeMultiPost, this.selectedItem).subscribe((result) => {
        this.selectedItem = [''];
        console.log(this.selectedItem);
        this.pagelist();

      }, (error) => {
        console.log(error);
      });

    }

    updatePageStatus(value) {

      // console.log(value);

      const  changeStatus = `${environment.apiUrl}changeStatus?status=${value}`;

      this.httpClient.post<any>(changeStatus, this.selectedItem).subscribe((response) => {
        console.log(response);
        this.pagelist();
      }, (error) => {
        console.log(error);
      });

    }

    // function for pagination
    pageChange(event) {

      // assign page value
      this.CurrentPage = event.pageIndex + 1;
      // assign the limit
      this.limit = event.pageSize;
      // page list called when value is changed every time
      this.pagelist();

    }



    // edit Page
    editPage(id) {

      this.router.navigate(['editpage/' + id]);

    }

    applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }


    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.length;

      // console.log(this.selection.selected);

      this.selectedData(this.selection.selected);


      return numSelected === numRows;

    }

      // data which is checked
      selectedData(slectedArray) {
        this.selectedItem = [];
        this.selectedItem = slectedArray;
      }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      console.log('check');
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.forEach(row => this.selection.select(row));
    }


  ngOnInit() {
    this.pagelist();
  }

}


