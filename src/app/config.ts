import  { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable ()

export class AuthInterceptor implements HttpInterceptor{

    loggiedin;
    constructor(private cookieService: CookieService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headers = new HttpHeaders({ 'Content-Type': '*'});
        const Token =this.cookieService.get('token');

        if (Token) {

            this.loggiedin = true;
            
            const cloned = req.clone({

               
                headers: req.headers.set('Authorization', 'Bearer ' + Token)
                
            });

            return next.handle(cloned)

        } else {
          
          return  next.handle(req);
        }   

    }
}