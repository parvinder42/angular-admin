import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, throwMatDialogContentAlreadyAttachedError} from '@angular/material';
import { Sidebar } from '../../components/sidebar/sidebar';
import { HttpClient } from '@angular/common/http';

import {environment} from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editrole',
  templateUrl: './editrole.component.html',
  styleUrls: ['./editrole.component.scss']
})


export class EditroleComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];


  category = [];
  selectedCategory = [];

  form: any = {

    permissions: []
  };

  private getRoleInfo = `${environment.apiUrl}getSingleRole`;
  private editRoleInfo = `${environment.apiUrl}editRole`;
  constructor(private fb: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private httpClient: HttpClient) { }

  roleData() {
      const id = {
        _id: this.activatedRoute.snapshot.params.post_id
      };

    this.httpClient.post(this.getRoleInfo, id).subscribe((response) => {

      console.log(response);
      this.form = response['result'];
      this.selectedCategory = response['result'].permissions;

        this.selectedCategory.forEach((es) => {
        this.category.splice(this.category.indexOf(es), 1);

      });



    }, (error) => {
      console.log(error);
    });
  }

  editRole() {

    this.form._id = this.activatedRoute.snapshot.params.post_id;
    this.form.permissions = this.selectedCategory;

    this.httpClient.post(this.editRoleInfo, this.form).subscribe((response) => {

      console.log(response);

    }, (error) => {
      console.log(error);
    });


  }


  listofCategory() {

    for (let k = 0; k < Sidebar.length; k++) {
      this.category.push(Sidebar[k].title);
    }
    console.log(this.category);

  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
    }

    if (input) {
      input.value = '';
    }
  }


  remove(category: string): void {

    const index = this.selectedCategory.indexOf(category);

    if (index >= 0) {
      this.selectedCategory.splice(index, 1);
      this.category.push(category);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {

    this.selectedCategory.push(event.option.viewValue);
    this.category.splice( this.category.indexOf(event.option.viewValue), 1 );
  }

  ngOnInit() {

    this.roleData();
    this.listofCategory();

  }

}
