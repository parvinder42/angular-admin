import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, throwMatDialogContentAlreadyAttachedError} from '@angular/material';
import { Sidebar } from '../../components/sidebar/sidebar';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-addrole',
  templateUrl: './addrole.component.html',
  styleUrls: ['./addrole.component.scss']
})
export class AddroleComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];


  ParentCategory = [];
  secondarySidebar = [];

  addRoleForm: FormGroup;

  form: any = {

    permissions: []
  };

  private role = `${environment.apiUrl}/addRole`;
  constructor(private fb: FormBuilder, private httpClient: HttpClient ) { }

  listofParentCategory() {

    // console.log(Sidebar);

    // this.ParentCategory=Sidebar;
    for (let k = 0; k < Sidebar.length; k++) {
      this.ParentCategory.push(Sidebar[k].title);
    }
    console.log(this.ParentCategory);

  }



  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
    }

    if (input) {
      input.value = '';
    }
  }


  remove(category: string): void {

    const index = this.secondarySidebar.indexOf(category);

    if (index >= 0) {
      this.secondarySidebar.splice(index, 1);
      this.ParentCategory.push(category);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {

    this.secondarySidebar.push(event.option.viewValue);
    this.ParentCategory.splice( this.ParentCategory.indexOf(event.option.viewValue), 1 );
  }

  // add role addRoleForm
  submitAddRole() {
this.form.permissions = this.secondarySidebar;
console.log(this.form);

    this.httpClient.post(this.role, this.form).subscribe((response) => {
      console.log(response);
    }, (error) => {
      console.log(error);
    });


  }


  ngOnInit() {
    this.listofParentCategory();

    this.addRoleForm = this.fb.group({

      role: ['', [Validators.required]],
      status: ['', [Validators.required]],
      permissions: this.fb.array([ this.fb.control('')])

    });

  }
  // createPermissions():FormGroup{

  //   return this.fb.group({

  //     data:''

  //   })

  // }
}
