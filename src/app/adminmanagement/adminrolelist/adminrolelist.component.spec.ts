import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrolelistComponent } from './adminrolelist.component';

describe('AdminrolelistComponent', () => {
  let component: AdminrolelistComponent;
  let fixture: ComponentFixture<AdminrolelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminrolelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminrolelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
