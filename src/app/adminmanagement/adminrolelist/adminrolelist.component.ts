import { Component, OnInit, ViewChild } from '@angular/core';
import {environment} from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {PageEvent} from '@angular/material';

@Component({
  selector: 'app-adminrolelist',
  templateUrl: './adminrolelist.component.html',
  styleUrls: ['./adminrolelist.component.scss']
})
export class AdminrolelistComponent implements OnInit {
  displayedColumns: string[] = ['select', 'Role', 'Permissions', 'Status', 'Action'];
  dataSource;
  CurrentPage;
  limit;
  totalLength;
  selectedItem = [];
  pageEvent: PageEvent;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  selection = new SelectionModel(true, []);
  private removeRole = `${environment.apiUrl}deleteSingleRole`;
  constructor(private httpClient: HttpClient) { }

  // list of roles assign to admin
  adminRoleList() {
    const list = `${environment.apiUrl}adminRoleList?page=${this.CurrentPage || 1}&limit=${this.limit || 5}`;

    this.httpClient.get<any>(list).subscribe((response) => {
      console.log(response);
      this.dataSource = response.data.data;
      this.dataSource.paginator = this.paginator;

      console.log(response.data.count.length);

      if (response.data.count.length > 0) {
        this.totalLength = response.data.count[0]['total'];
      } else {
        this.totalLength = 0;
      }
      console.log(response);
    }, (error) => {
      console.log(error);
    });
  }

// delete roles

  deleteRole(id) {

    const _id = {
      _id: id
    };

    this.httpClient.post(this.removeRole, _id).subscribe((response) => {

      console.log(response);
      this.adminRoleList();
    }, (error) => {
      console.log(error);
    });
  }

  pageChange(event) {

    // assign page value
    this.CurrentPage = event.pageIndex + 1;
    // assign the limit
    this.limit = event.pageSize;
    // page list called when value is changed every time
    this.adminRoleList();

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;

    // console.log(this.selection.selected);

    this.selectedData(this.selection.selected);


    return numSelected === numRows;

  }

    // data which is checked
    selectedData(slectedArray) {
      this.selectedItem = [];
      this.selectedItem = slectedArray;

    }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    console.log('check');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.forEach(row => this.selection.select(row));
  }

  quickAction() {

  }

  updatePageStatus(value) {

  }


  ngOnInit() {

    this.adminRoleList();
  }

}
