import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userdata;
  hide = true;
  hideNewPassword = true;
  userProfileForm:FormGroup
  resetPasswordForm:FormGroup

  private getProfile = `${environment.apiUrl}/get_profile`;
  private resetPassword = `${environment.apiUrl}/update_password`;
  private UpdateProfile = `${environment.apiUrl}/updateUserProfile`;
  constructor(private httpClient:HttpClient, private fb: FormBuilder ) { }

  // get user profile informatino 
  userProfile(){

    this.httpClient.get<any>(this.getProfile).subscribe((response)=>{

     
      this.userdata=response.data
      // set the value in userprofile form group
      this.userProfileForm.setValue({

        firstname: this.userdata.firstname,
        lastname: this.userdata.lastname,
        username:this.userdata.username,
        email:this.userdata.email,
        mobile:this.userdata.mobile,
        address:this.userdata.address
      })

    },(err)=>{
      console.log(err);
    })

  }

  // update password 
  updatePassword(){

    this.httpClient.post<any>(this.resetPassword, this.resetPasswordForm.value).subscribe((response)=>{
      console.log(response);
    },(error)=>{
      console.log(error);
    })
  }

  // function for update the userprofile
  updateUserProfile(){

    this.httpClient.post<any>(this.UpdateProfile, this.userProfileForm.value).subscribe((response)=>{
      this.userProfile();
    },(error)=>{
      console.log(error);
    })
  }


  ngOnInit() {
    this.userProfile();
    
    
    this.userProfileForm=this.fb.group({

      firstname:['',[Validators.required]],
      lastname:['',[Validators.required]],
      username:['',[Validators.required]],
      mobile:['', [Validators.required]],
      email:['',[Validators.email]],
      address:['',[Validators.required]]

    
    })

    this.resetPasswordForm=this.fb.group({

      password:['',[Validators.required]],
      newpassword:['',[Validators.required, Validators.minLength(6)]]
    })
  }

}
