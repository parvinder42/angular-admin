import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-editblogs',
  templateUrl: './editblogs.component.html',
  styleUrls: ['./editblogs.component.scss']
})
export class EditblogsComponent implements OnInit {
  private getSinglePost = `${environment.apiUrl}/getSinglePost`;
  private editPage = `${environment.apiUrl}/editPost`;
  private editPageWithFile = `${environment.apiUrl}/editPostWithImage`;

  editPageForm: FormGroup;
  selectedFile;
  imagePath;
  page: any = {};

  constructor(private fb: FormBuilder,
              private httpClient: HttpClient,
              private activatedRoute: ActivatedRoute,
              private router: Router
  ) { }

  // get the form data from the database
  pageData() {

    const id = {
      _id : this.activatedRoute.snapshot.params.post_id
    };


    this.httpClient.post(this.getSinglePost, id).subscribe((response) => {
      console.log(response['data']);

        this.imagePath = response['data'].image;
        console.log(this. imagePath);
        this.editPageForm.setValue({
          title: response['data'].title,
          short_desc: response['data'].short_desc,
          content: response['data'].content,
          meta_keyword: response['data'].meta_keyword,
          meta_title: response['data'].meta_title,
          meta_desc: response['data'].meta_desc,
          status: response['data'].status,
          type: ['blogs'],
          _id: this.activatedRoute.snapshot.params.post_id

        });

    }, (error) => {
      console.log(error);
    });
  }

  // edit page information
  editPageData() {

    this.httpClient.post(this.editPage, this.editPageForm.value).subscribe((response) => {
      console.log(response);
      this.router.navigate(['/blogs']);
    }, (error) => {
      console.log(error);
    });

  }

  // edit page with file
  editPageWithFileData() {

    if (this.editPageForm.valid) {

      const formData = new FormData();

      formData.append('title', this.editPageForm.value.title);
      formData.append('type', this.editPageForm.value.type);
      formData.append('short_desc', this.editPageForm.value.short_desc);
      formData.append('meta_keyword', this.editPageForm.value.meta_keyword);
      formData.append('content', this.editPageForm.value.content);
      // formData.append('meta_title', this.editPageForm.value.meta_title);
      formData.append('meta_desc', this.editPageForm.value.meta_desc);
      formData.append('_id', this.editPageForm.value._id);
      formData.append('status', this.editPageForm.value.status);

      if (this.selectedFile) {
        formData.append('file', this.selectedFile);

        this.httpClient.post<any>(this.editPageWithFile, formData).subscribe((response) => {
          console.log(response);
          this.router.navigate(['/blogs']);
        }, (error) => {
          console.log(error);
        });
      } else {
        this.editPageData();
      }
    }
  }



  // select file
  onSelectedFile (file) {
    if (file && file.target) {
      console.log(file);
       this.selectedFile = file.target.files[0];
    } else {
      this.selectedFile = null;
    }
  }


  // load the form
  formValue() {
    this.editPageForm = this.fb.group({

      title: ['', [Validators.required]],
      short_desc: [''],
      content: [''],
      meta_title: ['', [Validators.required]],
      meta_keyword: [''],
      meta_desc: [''],
      status: ['', [Validators.required]],
      type: ['blogs'],
      _id: ['']

    });
  }


  ngOnInit() {
    this.formValue();


    this.pageData();

  }

}
