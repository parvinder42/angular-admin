import { Component, OnInit , HostListener} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addblogs',
  templateUrl: './addblogs.component.html',
  styleUrls: ['./addblogs.component.scss']
})
export class AddblogsComponent implements OnInit {

  formAddContent: FormGroup;
  selectedFile;

  private pageAddWithImage = `${environment.apiUrl}/addPostWithImage`;
  private pageAdd = `${environment.apiUrl}/addPost`;
  constructor(private fb: FormBuilder, private httpClient: HttpClient, private router: Router) { }

    ele = document.querySelector('.file');

    onSelectedFile (file) {
      console.log(file.target.files[0]);
      if (file && file.target) {
         this.selectedFile = file.target.files[0];
      } else {
        this.selectedFile = null;
      }
    }


      addPage() {

        this.httpClient.post<any>(this.pageAdd, this.formAddContent.value).subscribe((response) => {
          console.log(response);
        }, (error) => {
          console.log(error);
        });

      }

      addPageWithFile() {

      // console.log(this.selectedFile);

      if (this.formAddContent.valid) {

        const formData = new FormData();

        formData.append('title', this.formAddContent.value.title);
        formData.append('type', this.formAddContent.value.type);
        formData.append('short_desc', this.formAddContent.value.short_desc);
        formData.append('meta_keyword', this.formAddContent.value.meta_keyword);
        formData.append('content', this.formAddContent.value.content);
        formData.append('meta_desc', this.formAddContent.value.meta_desc);
        formData.append('meta_title', this.formAddContent.value.meta_title);
        formData.append('status', this.formAddContent.value.status);

        if ( this.selectedFile) {
          formData.append('file', this.selectedFile);

          this.httpClient.post<any>(this.pageAddWithImage, formData).subscribe((response) => {
            console.log(response);
            this.router.navigate(['blogs']);
          }, (error) => {
            console.log(error);
          });
        } else {

          this.addPage();
        }
      }
    }


  ngOnInit() {
    this.formAddContent = this.fb.group({


      title: ['', [Validators.required]],
      short_desc: [''],
      content: [''],
      meta_keyword: [''],
      meta_desc: [''],
      meta_title: ['', Validators.required],
      status: ['', Validators.required],
      type: ['blogs']

    });
  }
}
