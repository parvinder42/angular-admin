import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-addadmin',
  templateUrl: './addadmin.component.html',
  styleUrls: ['./addadmin.component.scss']
})
export class AddadminComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];



  adminForm: FormGroup;
  private rolelistinfo = `${environment.apiUrl}rolelist`;
  private registerAdmin = `${environment.apiUrl}registerUserAdmin`;
  adminRole;
  constructor(private fb: FormBuilder, private httpClient: HttpClient ) { }

  rolelist() {

    this.httpClient.get(this.rolelistinfo).subscribe((response) => {
      console.log(response['result']);
      this.adminRole = response['result'];
    }, (error ) => {
      console.log(error);
    });
  }

  check() {

    this.adminRole.forEach((element) => {

        if (element._id === this.adminForm.value.role_id) {

          console.log(element._id);

          this.adminForm.value.role = element.role;
        }
    } );

  }

  submitForm() {

    this.httpClient.post(this.registerAdmin, this.adminForm.value).subscribe((response) => {

      console.log(response);

    }, (error) => {
      console.log(error);
    });
  }


  ngOnInit() {

    this.adminForm = this.fb.group({

      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      address: ['', [Validators.required]],
      role: [''],
      role_id: ['', [Validators.required]],
      status: ['', [Validators.required]],
      password: ['', [Validators.required]],
      type: [2]
    });

    this.rolelist();

  }

}
