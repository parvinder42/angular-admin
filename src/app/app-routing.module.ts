import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
// import { DialogServiceComponent } from './services/dialog-service/dialog-service.component';
const routes: Routes = [
  {path: '', component: LoginComponent},

  {path: '', component: LayoutComponent, children: [{path: '', loadChildren: './layout/layout.module#LayoutModule'}]}
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
