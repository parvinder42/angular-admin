import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {components, routes} from './layout.routing';
import { MatrialModule } from '../material.module';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({

  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatrialModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule

  ],

  declarations: [components]
})
export class LayoutModule  implements OnInit {

  ngOnInit() {

  }


 }
