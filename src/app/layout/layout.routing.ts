import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { BlogsComponent } from '../blogs/blogs/blogs.component';
import { AddblogsComponent  } from '../blogs/addblogs/addblogs.component';
import { EditblogsComponent } from '../blogs/editblogs/editblogs.component';
import { ViewpageComponent } from '../pages/viewpage/viewpage.component';
import { EditpageComponent } from '../pages/editpage/editpage.component';
import { AddpageComponent } from '../pages/addpage/addpage.component';
import { AuthguardGuard } from '../authguard.guard';
import { FaqComponent } from '../faq/faq/faq.component';
import { AddfaqComponent } from '../faq/addfaq/addfaq.component';
import { ProfileComponent } from '../profile/profile.component';
import { AddadminComponent } from '../admin/addadmin/addadmin.component';
import { EditadminComponent } from '../admin/editadmin/editadmin.component';
import { ViewadminComponent } from '../admin/viewadmin/viewadmin.component';
import { AddroleComponent } from '../adminmanagement/addrole/addrole.component';
import { AdminrolelistComponent } from '../adminmanagement/adminrolelist/adminrolelist.component';
import { EditroleComponent } from '../adminmanagement/editrole/editrole.component';
import { CookieService } from 'ngx-cookie-service';
export const components = [DashboardComponent,
    BlogsComponent, AddblogsComponent, EditblogsComponent, ViewpageComponent,
    AddpageComponent, EditpageComponent, FaqComponent, AddfaqComponent, ProfileComponent,
    AddadminComponent, AddroleComponent, AdminrolelistComponent, EditroleComponent, EditadminComponent, ViewadminComponent];

export const entryComponents = [AddadminComponent];


    export const routes =  [
            {path: 'dashboard', component: DashboardComponent, canActivate: [AuthguardGuard], data: {title: 'Dashboard'}},
            {path: 'blogs', component: BlogsComponent, canActivate: [AuthguardGuard], data: {title: 'Blogs'}},
            {path: 'add-blog', component: AddblogsComponent, canActivate: [AuthguardGuard], data: {title: 'Blogs'}},
            {path: 'edit-blog/:post_id', component: EditblogsComponent, canActivate: [AuthguardGuard]},
            {path: 'pages', component: ViewpageComponent, canActivate: [AuthguardGuard], data: {title: 'Pages'}},
            {path: 'addpage', component: AddpageComponent, canActivate: [AuthguardGuard], data: {title: 'Pages'}},
            {path: 'editpage/:post_id', component: EditpageComponent, canActivate: [AuthguardGuard], data: {title: 'Pages'}},
            {path: 'faq', component: FaqComponent, canActivate: [AuthguardGuard], data: {title: 'Faq'}},
            {path: 'addfaq', component: AddfaqComponent, canActivate: [AuthguardGuard], data: {title: 'Faq'}},
            {path: 'profile', component: ProfileComponent, canActivate: [AuthguardGuard], data: {title: 'Dashboard'}},
            {path: 'add-user-admin', component: AddadminComponent, canActivate: [AuthguardGuard], data: {title: 'User Admin'}},
            {path: 'edit-user-admin', component: EditadminComponent, canActivate: [AuthguardGuard], data: {title: 'User Admin'}},
            {path: 'view-user-admin', component: ViewadminComponent, canActivate: [AuthguardGuard], data: {title: 'User Admin'}},
            {path: 'add-admin-role', component: AddroleComponent, canActivate: [AuthguardGuard], data: {title: 'Admin Management'}},
            {path: 'admin-role-list', component: AdminrolelistComponent, canActivate: [AuthguardGuard], data: {title: 'Admin Management'}},
            {path: 'edit-role/:post_id', component: EditroleComponent, canActivate: [AuthguardGuard], data: {title: 'Admin Management'}}
           ];
