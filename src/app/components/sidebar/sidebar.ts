export const Sidebar = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        icon: 'dashboard',
    },
    {
        title: 'Blogs',
        icon: 'content_paste',
        class: '',
        id: 1,
        submenu: [
            {

                title: 'Add Blog',
                path: '/add-blog',
                icon: 'add_circle_outline',
                id: 1.1,
                parentid: 1
            },
            {
                title: 'Blog List',
                path: '/blogs',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
         title: 'Pages',
         icon: 'pages',
         class: '',
         id: 2,
         submenu: [
             {

                title: 'Add Page',
                path: '/addpage',
                icon: 'add_circle_outline',
                id: 2.1,
                parentid: 2,

             },
             {
                 title: 'Page List',
                 path: '/pages',
                 icon: 'list',
                 id: 2.2,
                parentid: 2,
             }
         ]
    },
    {
        title: 'Categories',
        icon: 'content_paste',
        class: '',
        id: 1,
        submenu: [
            {

                title: 'Add Category',
                path: '/categories/add',
                icon: 'add_circle_outline',
                id: 1.1,
                parentid: 1
            },
            {
                title: 'Categories List',
                path: '/categories',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
        title: 'Library',
        icon: 'content_paste',
        class: '',
        id: 1,
        submenu: [
            {

                title: 'Add Library',
                path: '/library/add',
                icon: 'add_circle_outline',
                id: 1.1,
                parentid: 1
            },
            {
                title: 'Library List',
                path: '/library',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
        title: 'Services',
        icon: 'content_paste',
        class: '',
        id: 3,
        submenu: [
            {

                title: 'Add service',
                path: '/services/add',
                icon: 'add_circle_outline',
                id: 3.1,
                parentid: 3
            },
            {
                title: 'Services List',
                path: '/services',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
        title: 'Testimonials',
        icon: 'content_paste',
        class: '',
        id: 1,
        submenu: [
            {

                title: 'Add Testimonial',
                path: '/testimonials/add',
                icon: 'add_circle_outline',
                id: 1.1,
                parentid: 1
            },
            {
                title: 'Testimonials List',
                path: '/testimonials',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
        title: 'Cases',
        icon: 'content_paste',
        class: '',
        id: 1,
        submenu: [
            {

                title: 'Add Case',
                path: '/cases/add',
                icon: 'add_circle_outline',
                id: 1.1,
                parentid: 1
            },
            {
                title: 'Cases List',
                path: '/cases',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
        title: 'Awards',
        icon: 'content_paste',
        class: '',
        id: 1,
        submenu: [
            {

                title: 'Add Award',
                path: '/awards/add',
                icon: 'add_circle_outline',
                id: 1.1,
                parentid: 1
            },
            {
                title: 'Award List',
                path: '/awards',
                icon: 'list',
                id: 1.2,
                parentid: 1

            }
        ]
    },
    {
        title: 'Settings',
        icon: 'content_paste',
        class: '',
        id: 1
    }
];
