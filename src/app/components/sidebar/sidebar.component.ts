
import { Component, OnInit } from '@angular/core';
import { Sidebar } from './sidebar';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})


export class SidebarComponent implements OnInit {
  menuItems: any[];

  permissionSidebar = [];
  displaySidebar = [];
  constructor(private cookieService: CookieService, private router: Router) {}


  logout() {
    this.cookieService.delete('token');
    this.router.navigate(['']);
  }

  loadSidebar() {
    this.displaySidebar = [];

    this.displaySidebar = Sidebar;
     this.displaySidebar.forEach((data) => {
      if (data.path) {
      } else if (data.submenu) {
        data.submenu.forEach((data2) => {
        });
      }

    });
  }

  ngOnInit() {
    this.loadSidebar();
    // this.menuItems = this.displaySidebar;
    this.menuItems = Sidebar;
  }

}

