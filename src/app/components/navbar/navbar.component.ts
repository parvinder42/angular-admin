import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private cookieService: CookieService, private router:Router) { }

  // logout the user
  logout(){
      this.cookieService.delete("token");

      this.router.navigate(['']);
  }

  ngOnInit() {

        

  }

}
